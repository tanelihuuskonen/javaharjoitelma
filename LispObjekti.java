import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.LinkedList;

enum LispObjektiTyyppi {
	NIL,
	FUNKTIO,
	ATOMI,
	KOKLUKU,
	MJONO,
	TOTUUSARVO,
	PARI
}


interface LispObjekti
{
  public LispObjektiTyyppi haeTyyppi( );
  public LispObjekti haeArvo( ) throws LispException;
}

interface LispFunktio1< T extends LispObjekti >
{
  T funktio( T syote ) throws LispException;
}

interface LispObjektiAtomi extends LispObjekti
{
  public String haeNimi( );
}

interface LispObjektiMjono extends LispObjekti
{
  public String haeSisalto( );
}

interface LispObjektiKokluku extends LispObjekti
{
  public int haeLukuarvo( );
}

interface LispObjektiTotuusarvo extends LispObjekti
{
  public boolean haeTotuusarvo( );
}

interface LispObjektiPari extends LispObjekti
{
  public LispObjekti car( );
  public LispObjekti cdr( );
}

class LispObjektiTot implements LispObjekti
{
  LispObjektiTyyppi tyyppi;

  public LispObjektiTyyppi haeTyyppi( )
  {
	return tyyppi;
  }

  public LispObjektiTot haeArvo( )
	throws LispException
  {
	return this;
  }

  public LispObjektiTot car( )
	throws LispException
  {
	throw new LispTyyppiException( );
  }

  public LispObjektiTot cdr( )
	throws LispException
  {
	throw new LispTyyppiException( );
  }

  public LispObjektiTot haeArvotRekursiivisesti( )
	throws LispException
  {
	throw new LispTyyppiException( );
  }

  public LispObjektiTot sovella( LispObjektiTot syote )
	throws LispException
  {
	throw new LispMaarittelematonException( );
  }

  public LispObjektiTot sovellaRekursiivisesti( LispFunktio1Tot f )
	throws LispException
  {
	throw new LispMaarittelematonException( );
  }
}

class LispFunktio1Tot implements LispFunktio1< LispObjektiTot >
{
  private LispFunktio1< LispObjektiTot > f1;


  @Override
  public LispObjektiTot funktio( LispObjektiTot arg )
	throws LispException
  {
	return f1.funktio( arg );
  }

  public LispFunktio1Tot( LispFunktio1< LispObjektiTot > f )
  {
	f1 = f;
  }
}

class LispObjektiNilTot extends LispObjektiTot implements LispObjekti
{
  private LispObjektiNilTot( )
  {
	tyyppi = LispObjektiTyyppi.NIL;
  }

  public static final LispObjektiNilTot lispNil = new LispObjektiNilTot( );

  @Override
  public String toString( )
  {
	return "()";
  }

  @Override
  public LispObjektiTot haeArvotRekursiivisesti( )
  {
	return lispNil;
  }


  public LispObjektiTot sovellaRekursiivisesti( LispFunktio1Tot f )
  {
	return lispNil;
  }
}

class LispObjektiFunktioTot
	extends LispObjektiTot
{
  LispFunktio1Tot ydin;

  public LispObjektiFunktioTot( )
  {
	tyyppi = LispObjektiTyyppi.FUNKTIO;
	ydin = null;
  }

  public LispObjektiFunktioTot( LispFunktio1Tot f1 )
  {
	tyyppi = LispObjektiTyyppi.FUNKTIO;
	ydin = f1;
  }

  public LispObjektiFunktioTot( LispFunktio1< LispObjektiTot > f1 )
  {
	tyyppi = LispObjektiTyyppi.FUNKTIO;
	ydin = new LispFunktio1Tot( f1 );
  }

  public LispObjektiTot sovella( LispObjektiTot syote )
	throws LispException
  {
	return ydin.funktio( syote );
  }
}

class LispObjektiFunktioArvoTot
	extends LispObjektiFunktioTot
{
  LispObjektiFunktioArvoTot( LispFunktio1Tot f1 )
  {
	super( f1 );
  }

  LispObjektiFunktioArvoTot( LispFunktio1< LispObjektiTot > f1 )
  {
	super( f1 );
  }

  @Override
  public LispObjektiTot sovella( LispObjektiTot syote )
	throws LispException
  {
	return ydin.funktio( syote.haeArvotRekursiivisesti( ) );
  }
}

class LispObjektiLambdaTot extends LispObjektiFunktioTot
{
  private LispObjektiTot parametrit;
  private LispObjektiTot lauseke;

  private static void tarkistaParametrit( LispObjektiTot p )
	throws LispException
  {
	LispObjektiTot pp;
	LispObjektiAtomiTot a;
	HashSet< String > nimet = new HashSet< String >( );
	String nimi;

	for (pp = p; LispObjektiPariTot.class.isInstance(pp); pp = pp.cdr()) {

		if (!LispObjektiAtomiTot.class.isInstance( pp.car( ) )) {
			throw new LispTyyppiException( );
		}

		a = (LispObjektiAtomiTot) (pp.car( ));
		nimi = a.haeNimi( );

		if (nimet.contains( nimi )) {
			throw new LispParametriException( );
		}

		nimet.add( nimi );
	}

	if (LispObjektiAtomiTot.class.isInstance( pp )) {
		a = (LispObjektiAtomiTot) pp;
		nimi = a.haeNimi( );
		pp = LispObjektiNilTot.lispNil;

		if (nimet.contains( nimi )) {
			throw new LispParametriException( );
		}
	}

	if (pp != LispObjektiNilTot.lispNil) {
		throw new LispParametriException( );
	}
  }

  public LispObjektiLambdaTot( LispObjektiTot p, LispObjektiTot l )
	throws LispException
  {
	tarkistaParametrit( p );
	parametrit = p;
	lauseke = l;
  }

  @Override
  public LispObjektiTot sovella( LispObjektiTot syote )
	throws LispException
  {
	LispObjektiAtomiTot a;
	LispObjektiTot s, p, tulos;
	String nimi;
	HashSet< String > tyhjatNimet = new HashSet< String >( );
	HashMap< String, LispObjektiTot > uudet
		= new HashMap< String, LispObjektiTot >( );
	HashMap< String, LispObjektiTot > talsi
		= new HashMap< String, LispObjektiTot >( );

	for (p = parametrit, s = syote.haeArvotRekursiivisesti( );
		LispObjektiPariTot.class.isInstance( p );
		p = p.cdr( ), s = s.cdr( )) {

		if (!LispObjektiAtomiTot.class.isInstance( p.car( ) )) {
			throw new LispTyyppiException( );
		}

		a = (LispObjektiAtomiTot)(p.car( ));
		nimi = a.haeNimi( );

		if (a.maaritelty( )) {
			talsi.put( nimi, a.haeArvo( ) );
		}
		else {
			tyhjatNimet.add( nimi );
		}

		uudet.put( nimi, s.car( ) );
	}

	if (LispObjektiAtomiTot.class.isInstance( p )) {
		a = (LispObjektiAtomiTot) p;
		nimi = a.haeNimi( );

		if (a.maaritelty( )) {
			talsi.put( nimi, a.haeArvo( ) );
		}
		else {
			tyhjatNimet.add( nimi );
		}

		uudet.put( nimi, s );
	}
	else {
		if (s != LispObjektiNilTot.lispNil) {
			throw new LispParametriException( );
		}
	}

	LispObjektiAtomiTot.lisaaArvot( uudet );
	tulos = lauseke.haeArvo( );
	LispObjektiAtomiTot.poistaArvot( tyhjatNimet );
	LispObjektiAtomiTot.lisaaArvot( talsi );
	return tulos;
  }
}

class LispObjektiAtomiTot extends LispObjektiTot implements LispObjektiAtomi
{
  String nimi;

  static private HashMap< String, LispObjektiTot > arvot
	= new HashMap< String, LispObjektiTot >( 100 );

  public LispObjektiAtomiTot( String s )
  {
	tyyppi = LispObjektiTyyppi.ATOMI;
	nimi = s;
  }

  public LispObjektiAtomiTot( String s, LispObjektiTot obj )
  {
	tyyppi = LispObjektiTyyppi.ATOMI;
	nimi = s;
	arvot.put( s, obj );
  }

  public static void poistaArvot( Set< String > nimet )
  {
	arvot.keySet( ).removeAll( nimet );
  }

  public static void lisaaArvot( Map< String, LispObjektiTot > uudet )
  {
	arvot.putAll( uudet );
  }

  public String haeNimi( )
  {
	return nimi;
  }

  public boolean maaritelty( )
  {
	return arvot.containsKey( nimi );
  }

  @Override
  public LispObjektiTot haeArvo( )
	throws LispException
  {
	if (arvot.containsKey( nimi )) {
		return arvot.get( nimi );
	}

	throw new LispMaarittelematonException( );
  }

  public void asetaArvo( LispObjektiTot obj )
  {
	arvot.put( nimi, obj );
  }

  @Override
  public String toString( )
  {
	return nimi;
  }

  @Override
  public int hashCode( )
  {
	return nimi.hashCode( );
  }

  private boolean equalsAt( LispObjektiAtomiTot obj )
  {
	return obj.haeNimi( ) == nimi;
  }

  private boolean equalsLisp( LispObjektiTot obj )
  {
	if (!LispObjektiAtomiTot.class.isInstance( obj )) {
		return false;
	}

	return this.equalsAt( (LispObjektiAtomiTot)obj );
  }

  @Override
  public boolean equals( Object obj )
  {
	if (LispObjektiTot.class.isInstance( obj )) {
		return equalsLisp( (LispObjektiTot)obj );
	}

	return false;
  }
}

class LispObjektiMjonoTot
	extends LispObjektiTot
	implements LispObjektiMjono
{
  String sisalto;

  public LispObjektiMjonoTot( String s )
  {
	tyyppi = LispObjektiTyyppi.MJONO;
	sisalto = s;
  }

  public String haeSisalto( )
  {
	return sisalto;
  }

  @Override
  public String toString( )
  {
	return '"' + sisalto + '"';
  }

  @Override
  public int hashCode( )
  {
	return sisalto.hashCode( );
  }

  private boolean equalsMj( LispObjektiMjonoTot obj )
  {
	return obj.haeSisalto( ) == sisalto;
  }

  private boolean equalsLisp( LispObjektiTot obj )
  {
	if (!LispObjektiMjonoTot.class.isInstance( obj )) {
		return false;
	}

	return this.equalsMj( (LispObjektiMjonoTot)obj );
  }

  @Override
  public boolean equals( Object obj )
  {
	if (LispObjektiTot.class.isInstance( obj )) {
		return equalsLisp( (LispObjektiTot)obj );
	}

	return false;
  }
}

class LispObjektiKoklukuTot
	extends LispObjektiTot
	implements LispObjektiKokluku
{
  int lukuarvo;

  public LispObjektiKoklukuTot( int l )
  {
	tyyppi = LispObjektiTyyppi.KOKLUKU;
	lukuarvo = l;
  }

  public int haeLukuarvo( )
  {
	return lukuarvo;
  }

  @Override
  public String toString( )
  {
	return Integer.toString( lukuarvo );
  }

  @Override
  public int hashCode( )
  {
	return lukuarvo;
  }

  private boolean equalsKok( LispObjektiKoklukuTot obj )
  {
	return obj.haeLukuarvo( ) == lukuarvo;
  }

  private boolean equalsLisp( LispObjektiTot obj )
  {
	if (!LispObjektiKoklukuTot.class.isInstance( obj )) {
		return false;
	}

	return this.equalsKok( (LispObjektiKoklukuTot)obj );
  }

  @Override
  public boolean equals( Object obj )
  {
	if (LispObjektiTot.class.isInstance( obj )) {
		return equalsLisp( (LispObjektiTot)obj );
	}

	return false;
  }
}

class LispObjektiTotuusarvoTot
	extends LispObjektiTot
	implements LispObjektiTotuusarvo
{
  boolean totuusarvo;

  private LispObjektiTotuusarvoTot( boolean t )
  {
	tyyppi = LispObjektiTyyppi.TOTUUSARVO;
	totuusarvo = t;
  }

  public boolean haeTotuusarvo ( )
  {
	return totuusarvo;
  }

  public static final LispObjektiTotuusarvoTot lispTosi
	= new LispObjektiTotuusarvoTot( true );

  public static final LispObjektiTotuusarvoTot lispEpatosi
	= new LispObjektiTotuusarvoTot( false );

  @Override
  public String toString( )
  {
	return totuusarvo ? "#t" : "#f";
  }
}

class LispObjektiPariTot extends LispObjektiTot implements LispObjektiPari
{
  LispObjektiTot car;
  LispObjektiTot cdr;

  public LispObjektiPariTot( LispObjektiTot a, LispObjektiTot d )
  {
	tyyppi = LispObjektiTyyppi.PARI;
	car = a;
	cdr = d;
  }

  public LispObjektiPariTot( LispObjektiTot a )
  {
	tyyppi = LispObjektiTyyppi.PARI;
	car = a;
	cdr = LispObjektiNilTot.lispNil;
  }

  @Override
  public LispObjektiTot car( )
  {
	return car;
  }

  @Override
  public LispObjektiTot cdr( )
  {
	return cdr;
  }

  public LinkedList< LispObjektiTot > listaksi( )
  {
	LinkedList< LispObjektiTot > akku = new LinkedList< LispObjektiTot >( );
	LispObjektiTot j;
	LispObjektiPariTot k;

	for (j = this; LispObjektiPariTot.class.isInstance( j ); ) {
		k = (LispObjektiPariTot) j;
		akku.add( k.car( ) );
		j = k.cdr( );
	}

	akku.add( j );
	return akku;
  }

  public LispObjektiTot sovellaRekursiivisesti( LispFunktio1Tot f )
	throws LispException
  {
	return new LispObjektiPariTot( f.funktio( car ),
					cdr.sovellaRekursiivisesti( f ) );
  }

  @Override
  public LispObjektiTot haeArvo( ) throws LispException
  {
	return car.haeArvo( ).sovella( cdr );
  }

  @Override
  public LispObjektiTot haeArvotRekursiivisesti( ) throws LispException
  {
	return new LispObjektiPariTot( car.haeArvo( ),
					cdr.haeArvotRekursiivisesti( ) );
  }

  private String suluttomaksi( )
  {
	String alku;
	LispObjektiPariTot k;

	alku = car.toString( );

	if (cdr == LispObjektiNilTot.lispNil) {
		return alku;
	}

	if (LispObjektiPariTot.class.isInstance( cdr )) {
		k = (LispObjektiPariTot) cdr;
		return alku + " " + k.suluttomaksi( );
	}

	return alku + " . " + cdr.toString( );
  }

  @Override
  public String toString( )
  {
	return "(" + suluttomaksi( ) + ")";
  }
}
