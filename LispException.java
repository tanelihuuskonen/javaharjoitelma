public class LispException extends Exception
{
}

class LispMaarittelematonException extends LispException
{
}

class LispTyyppiException extends LispException
{
}

class LispParametriException extends LispException
{
}

class LispParametriLiikaaException extends LispParametriException
{
}

class LispParametriPuuttuuException extends LispParametriException
{
}
