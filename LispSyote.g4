grammar LispSyote;
syote :		lauseke* EOF ;
rivi :		lauseke* osalauseke EOF ;
lauseke :	TOTUUSARVO	#totuusarvoLauseke
	|	KOKLUKU		#koklukuLauseke
	|	MJONO		#mjonoLauseke
	|	ATOMI		#atomiLauseke
	|	lista		#listaLauseke
	|	pistelista	#pistelistaLauseke
	|	lainaus 	#lainausLauseke ;
osalauseke :	// tyhja
	|	'(' lauseke* osalauseke
	|	'(' lauseke+ '.' lauseke
	|	'(' lauseke+ '.' osalauseke
	|	'\'' osalauseke ;
lista :		'(' lauseke* ')' ;
pistelista :	'(' lauseke+ '.' lauseke ')' ;
lainaus :	'\'' lauseke ;
fragment MJMERKKI :		~'"' ;
fragment ERIKOISMERKKI :	[-!#$%&|*+/:<=>?@^_~] ;
fragment KIRJAIN :		[a-zA-Z] ;
fragment NUMERO :		[0-9] ;
TOTUUSARVO :	'#' ('t' | 'T' | 'f' | 'F' ) ;
KOKLUKU :	'-'? NUMERO+ ;
MJONO :		'"' MJMERKKI* '"' ;
ATOMI :		(KIRJAIN | ERIKOISMERKKI) (KIRJAIN|ERIKOISMERKKI|NUMERO)* ;
TYHJA :		[ \t\r\n] -> skip ;
KOMMENTTI :	';' ~[\r\n]* -> skip ;
