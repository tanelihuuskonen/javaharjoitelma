class LispFunktiot
{
  private static final LispObjektiNilTot nil = LispObjektiNilTot.lispNil;
  private static final LispObjektiTotuusarvoTot tosi
	  = LispObjektiTotuusarvoTot.lispTosi;
  private static final LispObjektiTotuusarvoTot epatosi
	  = LispObjektiTotuusarvoTot.lispEpatosi;

  private static LispObjektiTot cadr( LispObjektiTot obj )
	throws LispException
  {
	try {
		return obj.cdr( ).car( );
	}
	catch (LispTyyppiException e) {
		throw new LispParametriPuuttuuException( );
	}
  }

  private static LispObjektiTot cddr( LispObjektiTot obj )
	throws LispException
  {
	try {
		return obj.cdr( ).cdr( );
	}
	catch (LispTyyppiException e) {
		throw new LispParametriPuuttuuException( );
	}
  }

  private static LispObjektiTot caddr( LispObjektiTot obj )
	throws LispException
  {
	try {
		return obj.cdr( ).cdr( ).car( );
	}
	catch (LispTyyppiException e) {
		throw new LispParametriPuuttuuException( );
	}
  }

  private static LispObjektiTot funktioCons( LispObjektiTot obj )
	throws LispException
  {
	if (cddr( obj ) != nil) {
		throw new LispParametriLiikaaException( );
	}

	return new LispObjektiPariTot( obj.car( ), cadr( obj ) );
  }

  private static LispObjektiTot funktioQuote( LispObjektiTot obj )
	throws LispException
  {
	if (obj.cdr( ) != nil) {
		throw new LispParametriLiikaaException( );
	}

	return obj.car( );
  }

  private static LispObjektiTot funktioLambda( LispObjektiTot obj )
	throws LispException
  {
	if (cddr( obj ) != nil) {
		throw new LispParametriLiikaaException( );
	}

	return new LispObjektiLambdaTot( obj.car( ), cadr( obj ) );
  }

  private static LispObjektiTot funktioCar( LispObjektiTot obj )
	throws LispException
  {
	if (obj.cdr( ) != nil) {
		throw new LispParametriLiikaaException( );
	}

	return obj.car( ).car( );
  }

  private static LispObjektiTot funktioCdr( LispObjektiTot obj )
	throws LispException
  {
	if (obj.cdr( ) != nil) {
		throw new LispParametriLiikaaException( );
	}

	return obj.car( ).cdr( );
  }

  private static LispObjektiTot funktioDefine( LispObjektiTot obj )
	throws LispException
  {
	if (cddr( obj ) != nil) {
		throw new LispParametriLiikaaException( );
	}

	if (!LispObjektiAtomiTot.class.isInstance( obj.car( ) )) {
		throw new LispParametriException( );
	}

	LispObjektiAtomiTot a = (LispObjektiAtomiTot) (obj.car( ));

	a.asetaArvo( cadr( obj ).haeArvo( ) );
	return nil;
  }

  private static LispObjektiTot funktioIf( LispObjektiTot obj )
	throws LispException
  {
	LispObjektiTot p = cddr( obj );

	if ((p != nil) && (p.cdr( ) != nil)) {
		throw new LispParametriLiikaaException( );
	}

	if (obj.car( ).haeArvo( ) != epatosi) {
		return cadr( obj ).haeArvo( );
	}

	if (p != nil) {
		return p.car( ).haeArvo( );
	}

	return nil;
  }

  private static LispObjektiTot funktioEqQ( LispObjektiTot obj )
	throws LispException
  {
	if (cddr( obj ) != nil) {
		throw new LispParametriLiikaaException( );
	}

	return obj.car( ).equals( cadr( obj ) ) ? tosi : epatosi ;
  }

  private static LispObjektiTot funktioPairQ( LispObjektiTot obj )
	throws LispException
  {
	if (obj.cdr( ) != nil) {
		throw new LispParametriLiikaaException( );
	}

	return (obj.car( ).haeTyyppi( ) == LispObjektiTyyppi.PARI)
		? tosi : epatosi ;
  }

  public static final LispObjektiFunktioTot lispQuote
	= new LispObjektiFunktioTot(
		(LispObjektiTot syote) -> funktioQuote( syote ) );

  public static final LispObjektiFunktioTot lispList
	= new LispObjektiFunktioArvoTot( (LispObjektiTot syote) -> syote );

  public static final LispObjektiFunktioTot lispCons
	= new LispObjektiFunktioArvoTot(
		(LispObjektiTot syote) -> funktioCons( syote ) );

  public static final LispObjektiFunktioTot lispCar
	= new LispObjektiFunktioArvoTot(
		(LispObjektiTot syote) -> funktioCar( syote ) );

  public static final LispObjektiFunktioTot lispCdr
	= new LispObjektiFunktioArvoTot(
		(LispObjektiTot syote) -> funktioCdr( syote ) );

  public static final LispObjektiFunktioTot lispLambda
	= new LispObjektiFunktioTot(
		(LispObjektiTot syote) -> funktioLambda( syote ) );

  public static final LispObjektiFunktioTot lispDefine
	= new LispObjektiFunktioTot(
		(LispObjektiTot syote) -> funktioDefine( syote ) );

  public static final LispObjektiFunktioTot lispIf
	= new LispObjektiFunktioTot(
		(LispObjektiTot syote) -> funktioIf( syote ) );

  public static final LispObjektiFunktioTot lispEqQ
	= new LispObjektiFunktioArvoTot(
		(LispObjektiTot syote) -> funktioEqQ( syote ) );

  public static final LispObjektiFunktioTot lispPairQ
	= new LispObjektiFunktioArvoTot(
		(LispObjektiTot syote) -> funktioPairQ( syote ) );
}
