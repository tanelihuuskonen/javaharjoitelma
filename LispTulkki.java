import java.util.Stack;
import java.io.InputStreamReader;
import java.io.BufferedReader;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.UnbufferedTokenStream;
import org.antlr.v4.runtime.UnbufferedCharStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;
//import org.antlr.v4.runtime.tree.ErrorNode;
//import org.antlr.v4.runtime.tree.TerminalNode;


class LispTulkkiLukija
	extends LispSyoteBaseListener
	implements LispSyoteListener
{
  private static final LispObjektiNilTot nil = LispObjektiNilTot.lispNil;
  private static final LispObjektiAtomiTot lispQuote
	= new LispObjektiAtomiTot( "quote", LispFunktiot.lispQuote );
  private static final LispObjektiAtomiTot lispList
	= new LispObjektiAtomiTot( "list", LispFunktiot.lispList );
  private static final LispObjektiAtomiTot lispCons
	= new LispObjektiAtomiTot( "cons", LispFunktiot.lispCons );
  private static final LispObjektiAtomiTot lispCar
	= new LispObjektiAtomiTot( "car", LispFunktiot.lispCar );
  private static final LispObjektiAtomiTot lispCdr
	= new LispObjektiAtomiTot( "cdr", LispFunktiot.lispCdr );
  private static final LispObjektiAtomiTot lispLambda
	= new LispObjektiAtomiTot( "lambda", LispFunktiot.lispLambda );
  private static final LispObjektiAtomiTot lispDefine
	= new LispObjektiAtomiTot( "define", LispFunktiot.lispDefine );
  private static final LispObjektiAtomiTot lispIf
	= new LispObjektiAtomiTot( "if", LispFunktiot.lispIf );
  private static final LispObjektiAtomiTot lispEqQ
	= new LispObjektiAtomiTot( "eq?", LispFunktiot.lispEqQ );
  private static final LispObjektiAtomiTot lispPairQ
	= new LispObjektiAtomiTot( "pair?", LispFunktiot.lispPairQ );
  private static final LispObjektiTotuusarvoTot tosi
	= LispObjektiTotuusarvoTot.lispTosi;
  private static final LispObjektiTotuusarvoTot epatosi
	= LispObjektiTotuusarvoTot.lispEpatosi;

  private int tila = 0;
  private Stack< LispObjektiTot > lpino = new Stack< LispObjektiTot >( );
  private Stack< Stack< LispObjektiTot > > pinopino
	= new Stack< Stack< LispObjektiTot > >( );

  private boolean tulostus = false;
  int jaannosindeksi = -1;

  LispTulkkiLukija( boolean t ) {
	tulostus = t;
  }

  private void kasittele( LispObjektiTot obj )
  {
	if (tila == 0) {
		try {
			if (tulostus) {
				System.out.println( obj.haeArvo( ) );
			}
			else {
				obj.haeArvo( );
			}
		}
		catch (LispParametriLiikaaException e) {
			System.err.println( "ylimaarainen parametri" );
		}
		catch (LispParametriPuuttuuException e) {
			System.err.println( "parametri puuttuu" );
		}
		catch (LispParametriException e) {
			System.err.println( "parametrivirhe" );
		}
		catch (LispMaarittelematonException e) {
			System.err.println( "maarittelematon muuttuja" );
		}
		catch (LispTyyppiException e) {
			System.err.println( "tyyppivirhe" );
		}
		catch (LispException e) {
			System.err.println( "tuntematon virhe" );
		}
	}
	else {
		lpino.push( obj );
	}
  }

  private LispObjektiTot kokoaLista( LispObjektiTot loppu )
  {
	LispObjektiTot lista = loppu;

	while( lpino.size( ) > 0) {
		lista = new LispObjektiPariTot( lpino.pop( ), lista );
	}

	return lista;
  }

  @Override
  public void enterLainaus(
		@NotNull LispSyoteParser.LainausContext ctx )
  {
	tila++;
  }

  @Override
  public void exitLainaus(
		@NotNull LispSyoteParser.LainausContext ctx )
  {
	LispObjektiTot arg = new LispObjektiPariTot( lpino.pop( ), nil );
	tila--;
	kasittele( new LispObjektiPariTot( lispQuote, arg ) );
  }

  @Override
  public void exitAtomiLauseke(
		@NotNull LispSyoteParser.AtomiLausekeContext ctx )
  {
	String nimi = ctx.getText( );
	kasittele( new LispObjektiAtomiTot( ctx.getText( ) ) );
  }

  @Override
  public void exitTotuusarvoLauseke(
		@NotNull LispSyoteParser.TotuusarvoLausekeContext ctx )
  {
	boolean tot = ctx.getText( ).equalsIgnoreCase( "#t" );
	kasittele( tot ? tosi : epatosi );
  }

  @Override
  public void exitMjonoLauseke(
		@NotNull LispSyoteParser.MjonoLausekeContext ctx )
  {
	String s = ctx.getText( );
	s = s.substring( 1, s.length( ) - 1 );
	kasittele( new LispObjektiMjonoTot( s ) );
  }

  @Override
  public void exitKoklukuLauseke(
		@NotNull LispSyoteParser.KoklukuLausekeContext ctx )
  {
	int j;

	j = Integer.valueOf( ctx.getText( ) );
	kasittele( new LispObjektiKoklukuTot( j ) );
  }

  @Override
  public void enterLista( @NotNull LispSyoteParser.ListaContext ctx )
  {
	pinopino.push( lpino );
	lpino = new Stack< LispObjektiTot >( );
	tila++;
  }

  @Override
  public void exitLista( @NotNull LispSyoteParser.ListaContext ctx )
  {
	LispObjektiTot lista = kokoaLista( nil );
	lpino = pinopino.pop( );
	tila--;
	kasittele( lista );
  }

  @Override
  public void enterPistelista( @NotNull LispSyoteParser.PistelistaContext ctx )
  {
	pinopino.push( lpino );
	lpino = new Stack< LispObjektiTot >( );
	tila++;
  }

  @Override
  public void exitPistelista( @NotNull LispSyoteParser.PistelistaContext ctx )
  {
	LispObjektiTot lista = kokoaLista( lpino.pop( ) );
	lpino = pinopino.pop( );
	tila--;
	kasittele( lista );
  }

  @Override
  public void enterOsalauseke( @NotNull LispSyoteParser.OsalausekeContext ctx )
  {
	tulostus = false;
  }

  @Override
  public void exitOsalauseke( @NotNull LispSyoteParser.OsalausekeContext ctx )
  {
	jaannosindeksi = ctx.start.getCharPositionInLine( );
  }
}

public class LispTulkki
{
  public static void main( String[ ] args ) throws Exception
  {
	CharStream syote;
	String osa = "";
	boolean jatkuu = true;
	String rivi;

	for (int j = 0; j < args.length; j++) {
		syote = new ANTLRFileStream( args[ 0 ] );
		LispSyoteLexer lex = new LispSyoteLexer( syote );
		CommonTokenStream tok = new CommonTokenStream( lex );
		LispSyoteParser p = new LispSyoteParser( tok );
		p.addParseListener( new LispTulkkiLukija( false ) );
		ParserRuleContext t = p.syote( );
	}

	BufferedReader standardiLukija
		= new BufferedReader( new InputStreamReader( System.in ) );

	while( jatkuu ) {
		try {
			rivi = standardiLukija.readLine( );

			if (rivi == null) {
				jatkuu = false;
				break;
			}

			rivi = osa + rivi + " ";
			syote = new ANTLRInputStream( rivi );
		}
		catch (Exception e) {
			jatkuu = false;
			break;
		}

		LispSyoteLexer lex = new LispSyoteLexer( syote );
		CommonTokenStream tok = new CommonTokenStream( lex );
		LispSyoteParser p = new LispSyoteParser( tok );
		LispTulkkiLukija l = new LispTulkkiLukija( true );
		p.addParseListener( l );
		ParserRuleContext t = p.rivi( );
		osa = rivi.substring( l.jaannosindeksi );
	}
  }
}
